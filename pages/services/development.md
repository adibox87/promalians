---
layout: service
title: Разработка технической документации
slug: development
---

## Технические условия
**Технические условия (ТУ)** – это документ, который должен содержать полный комплекс требований к продукции, ее изготовлению, транспортировке и хранению, указания по эксплуатации, контролю и приемке.

ТУ являются техническим документом, который разрабатывается по решению разработчика (изготовителя) или по требованию заказчика (потребителя) продукции. Это неотъемлемая часть комплекта конструкторской или другой технической документации на продукцию. Технические условия разрабатывают на одно конкретное изделие, материал, вещество или несколько конкретных изделий, материалов, веществ и т. п. Требования, установленные ТУ, не должны противоречить обязательным требованиям государственных (межгосударственных) стандартов, распространяющихся на данную продукцию.

Технические условия должны соответствовать ГОСТ 2.114-2016. При регистрации технических условий на титульный лист и на каталожный лист наносится печать и отметки регистрирующей организации. Производить регистрацию и внесение в реестр технических условий имеет право только региональные аккредитованные Росстандартом организации. Технические условия должны содержать вводную часть и разделы, расположенные в следующей последовательности: технические требования, требования безопасности, требования охраны окружающей среды, правила приемки, методы контроля, транспортирование и хранение, указания по эксплуатации, гарантии изготовителя.

### Согласование и утверждение технических условийСогласование и утверждение технических условий

Документ в обязательном порядке подлежит утверждению. Разработчик соглашается со спецификациями заказчика (потребителя). Необходимость проведения процедуры согласования ТУ с другими заинтересованными организациями определяет разработчик совместно с заказчиком (потребителем). Срок согласования не должен превышать 20 дней с момента поступления в их организацию. Соответствие документа оформляют подписью руководителя (заместителя руководителя) координирующей организации штампом «Проведен» и в отдельном документе (акте приемочной комиссии, письме, отчете и т. д.). Утверждение спецификаций осуществляется разработчиком документа или органом, предусматривающим действующее законодательство. Утверждение технических условий на продукцию оформляется подписью руководителя (при необходимости заместителем руководителя) надписью "УТВЕРЖДАЮ" на титульном листке документов.

ТУ утверждаются, как правило, на неопределенный срок. Изменения относительно технических условий согласовываются и утверждаются обычно в том же порядке. Владельцем является предприятие, которое разрабатывает и утверждает технические условия, оно же является держателем подлинника. Регистрация технических условий информационного обеспечения потребителям (заказчикам) ассортимента и качества на выпускаемую продукцию, информация об утвержденных технических условиях на товарную продукцию отправляется на регистрацию в Росстандарт (Федеральное агентство по техническому регулированию). Процедура регистрации технических условий не обязательна, за исключением регистрации технических условий для химической, пищевой, фармацевтической и косметической продукции.

Регистрация ТУ решает проблему интеллектуальной собственности, а также упрощает во многом процедуру сертификации продукции. Только аккредитованные Росстандартом организации имеют право регистрации по месту нахождения предприятия и записи в реестре спецификации. При регистрации технических условий на титульный лист, как правило, наносят отметки для регистрирующей организации. Сроки и стоимость услуги разработки технических условий определяются в каждом конкретном случае и зависят от вида продукции и имеющейся документации.

**Перечень необходимых данных для разработки технических условий**

1. код ОКПО изготовителя;
2. наименование изделия,
3. перечень модификаций;
4. назначение изделия, подробное техническое описание и описание применения;
5. код изделия по Общероссийскому классификатору продукции (ОКП);
6. описание технологического процесса;
7. полный перечень технических параметров;
8. перечень комплектующих изделий; порядок и условия предъявления и приемки продукции органами технического контроля предприятия-изготовителя;
9. методы и средства контроля испытаний;
10. способы упаковки и упаковочный материал, перечень документов, вкладываемых в упаковку;
11. транспортировка (виды транспорта и транспортного средства, параметры транспортировки);
12. условия хранения;
13. требования безопасности при изготовлении и эксплуатации изделия;
14. условия эксплуатации;
15. сроки гарантии.

## Паспорт на продукцию

**Паспорт (или формуляр) технического устройства** является видом технической документации предприятия и в **обязательном** порядке должен содержать данные и сведения, которые должным образом подтверждают гарантию производителя на данный вид продукции.

В паспорте технического устройства приводятся основные значения характеристик и параметров, свойства данного изделия либо вида продукции, пути ее утилизации, информация о выданных сертификатах.

Паспорт технического устройства (ТУ) относится к эксплуатационной документации и разрабатывается на конкретное изделие.

Паспорта на некоторые технические изделия нормируются для соответствующих отраслей промышленности (пример: паспорт на сосуды, работающие под давлением; грузоподъемные механизмы; котлы; лифты; станки, трубопроводы).

В соответствии, с рядом нормативных правовых актов, наличие паспорта на техническое устройство обязательно, при отсутствии паспорта эксплуатация технического устройства запрещена.

### Паспорт технического устройства на изделие состоит из следующих разделов

1. основные сведения об изделии и технические данные;
2. комплектность;
3. ресурсы, сроки службы и хранения и гарантии изготовителя (поставщика);
4. консервация;
5. свидетельство об упаковывании;
6. свидетельство о приемке;
7. движение изделия в эксплуатации (при необходимости);
8. ремонт и учет работы по бюллетеням и указаниям (при необходимости);
9. заметки по эксплуатации и хранению (при необходимости);
10. сведения об утилизации;
11. особые отметки;
12. сведения о способах заказа и условиях приобретения (при необходимости).

## Руководство по эксплуатации

**Руководство по эксплуатации (РЭ)** – документ нормативно-технического характера, который является частью пакета документации, прилагаемой к товарам, содержащим в себе сведения о конструктивных особенностях продукции, ее составных частях, принципе действия и требованиях по безопасности во время эксплуатации, ремонту и обслуживанию.

**Руководство по эксплуатации** разрабатывается производственным предприятием в соответствии с действующими стандартами, устанавливающими правила составления, оформления и регистрации конструкторской документации. Основными документами, регламентирующими разработку нормативно-технической документации для **законной реализации** продукции, являются:

- ГОСТ 2.601-95 – устанавливает основные требования по оформлению эксплуатационных документов;

- ГОСТ 2.105-95 – прописывает правила по составлению текстовых видов документации.

**Разработка РЭ** должна осуществляться на этапе подготовки процесса производства, и прилагаться ко всем видам изделий, поступающих в обращение на рынок страны. Следует отметить, что в определенных случаях руководство по эксплуатации требуется для оформления некоторых разрешительных документов. К примеру, РЭ напрямую относится к получению разрешения на применение оборудования на опасных производственных объектах, выдаваемого органами Ростехнадзора. В данном случае без руководства по эксплуатации соискателю будет отказано в оформлении разрешения на применение и, следовательно, техническое устройство не будет допущено к применению.

**Руководство по эксплуатации** – это, прежде всего, разъяснительный документ, который обязан обеспечивать покупателей всей основной информацией по использованию товаров, именно поэтому, в большинстве случаев, документ содержит в себе не только текстовую информацию, но и различные виды схем, рисунков, графиков и фотографий, которые позволяют визуально ознакомиться с теми или иными процессами в период эксплуатации продукции.

###  Руководство по эксплуатации должно содержать следующие обязательные разделы:

1. вводная часть;
2. содержание;
3. полное описание изделия и условия его функционирования;
4. сервисное обслуживание и ремонт;
5. возможные неисправности и способы их устранения;
6. правила по транспортировке, хранению и утилизации;
7. глоссарий, предметный указатель и другая необходимая информация.

## Паспорт качества
**Паспорт качества** на продукцию — технологический документ, представляющий расширенное описание характеристик конкретного товара, полученных в ходе лабораторных исследований. Паспорт качества не является заменой обязательной сертификации, положения и порядок его выдачи регулируются **ЕСТД, ГОСТ **и отраслевыми стандартами.

Бланки разрабатываются для определенных типов продукции с последующим тиражированием в необходимом количестве. После оформления каждый экземпляр паспорта качества заверяется должностными лицами.

### Цель использования паспорта качества

Для большинства видов сырья показатели ГОСТ и ТУ имеют широкий диапазон допустимых значений, что позволяет производить отдельные партии продукции с разными характеристиками. Это значительно усложняет покупателю выбор необходимого изделия или материала для конкретных нужд. Детальное описание фактических параметров продукции в паспорте качества позволяет покупателю подбирать товары, максимально соответствующие определенным условиям.

Паспорт качества оформляется в соответствии с методиками, разработанными согласно нормативам отраслевых стандартов.

**Срок подготовки документа составляет 1-2 дня.**

