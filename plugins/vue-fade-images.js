import Vue from 'vue'
import VueFadeImages from 'vue-fade-images'

Vue.component('vue-fade-images', VueFadeImages)
