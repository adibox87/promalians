import settings from './settings.js'

const { meta } = settings

export default {
  target: 'static',
  ssr: true,
  mode: 'universal',

  /*
   ** Headers of the page
   */
  head: {
    title: meta.title,
    link: [
      {
        rel: 'apple-touch-icon',
        sizes: '180x180',
        href: '/apple-touch-icon.png'
      },
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'mask-icon', color: '#c5517d', href: '/safari-pinned-tab.svg' },
      { rel: 'dns-prefetch', href: '//www.googletagmanager.com/' },
      { rel: 'preconnect', href: 'https://www.googletagmanager.com/' },
      { rel: 'dns-prefetch', href: '//fonts.googleapis.com' },
      {
        rel: 'preconnect',
        href: 'https://fonts.gstatic.com/',
        crossorigin: true
      },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css2?family=Montserrat:wght@900&text=D.1&display=swap'
      },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,500;0,700;1,400&display=swap'
      }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#e4b84d' },
  loadingIndicator: {
    name: 'cube-grid',
    color: '#e4b84d',
    background: '#3E3E3E'
  },
  /*
   ** Global CSS
   */
  css: ['~/assets/css/main.scss'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '~/plugins/vue-fade-images', mode: 'client' },
    { src: '~plugins/slick-slide.js', ssr: false, mode: 'client' },
    { src: '~/plugins/smooth-scroll', ssr: false, mode: 'client' }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    '@nuxtjs/stylelint-module',
    // https://github.com/Developmint/nuxt-svg-loader/
    'nuxt-svg-loader',
    '@nuxtjs/imagemin'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    '@nuxtjs/pwa',
    '@nuxtjs/markdownit',
    '@nuxt/press',
    '@nuxtjs/dotenv',
    '@nuxtjs/robots',
    '@nuxtjs/sitemap'
  ],

  pwa: {
    meta: {
      favicon: false,
      name: meta.title,
      description: meta.description,
      ogSiteName: meta.shortTitle
    },
    manifest: {
      name: meta.title,
      shortName: meta.shortTitle,
      description: meta.description
    }
  },

  markdownit: {
    injected: true
  },

  gtm: {
    dev: false,
    id: process.env.GTM_ID || 'GTM-XXXXXXX'
  },

  env: {
    GOOGLE_ANALYTICS_ID: process.env.GOOGLE_ANALYTICS_ID || 'UA-XXXXXXXX-X',
    GTM_ID: process.env.GTM_ID || 'GTM-XXXXXXX'
  },

  robots: {},

  sitemap: {
    hostname:
      process.env.URL || process.env.CI_PAGES_URL || 'http://localhost:3000'
  },
  /*
   ** Customize the base url
   */
  router: {
    base:
      (process.env.URL || process.env.CI_PAGES_URL) &&
      new URL(process.env.URL || process.env.CI_PAGES_URL).pathname
  },

  /*
   ** Customize the generated output folder
   */
  generate: {
    dir: 'public'
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */

    extend(config, ctx) {}
  }
}
